import React, { Component } from 'react';
import './main.css';
import AddNewTodo from './AddingTodo'
import FooterPart from './FooterPart';
import { v4 as uuidv4 } from 'uuid';

class TodoApp extends Component {
    state = {
        todosList: [],
        bottomBtn: 'showAll',
        theme: false
    }

    handelNewTodo = (text) => {
        if (text === '') {
            alert('Please Enter Todo!!')
        }
        else {
            let newTodo = {
                id: uuidv4(),
                givenTodo: text,
                isChecked: false
            }
            this.setState({
                todosList: [newTodo, ...this.state.todosList]
            })
        }
    }

    toggleCheckBox = (obj) => {
        obj.isChecked = obj.isChecked ? false : true
        this.setState({
            todosList: this.state.todosList
        })
    }

    deletion = (id) => {
        this.setState({
            todosList: this.state.todosList.filter(todo => (todo.id !== id))
        })

    }

    switchBtn = () => {
        if (this.state.bottomBtn === 'showAll') {
            return (this.state.todosList)
        }
        else if (this.state.bottomBtn === 'showActive') {
            return (this.state.todosList.filter((todo) => (!todo.isChecked)))
        }
        else if (this.state.bottomBtn === 'showComplete') {
            return (this.state.todosList.filter((todo) => (todo.isChecked)))
        }
        return []
    }

    handelFilters = (btn) => {
        this.setState({
            bottomBtn: btn.operation
        })
    }

    clearCompleted = () => {
        this.setState({
            todosList: this.state.todosList.filter((todo) => (!todo.isChecked))
        })
    }

    render() {
        const { todosList } = this.state
        const todosCount = (todosList.filter((l) => (!l.isChecked))).length
        const displayTodos = this.switchBtn()

        return (
            <div className="bg" id="main">
                <section className="light-mode">
                    <header className="head-part">
                        <h1 className="title">TODO</h1>
                        <div className="moon" id='moon'></div>
                    </header>

                    <div className='creating-section d-flex justify-content-center'>
                        <AddNewTodo handelNewTodo={this.handelNewTodo} />
                    </div>


                    <ul className="todos-container" id='todos-container'>
                        {
                            displayTodos.map((each) => {
                                return (
                                    <div key={each.id} className='d-flex'>
                                        <input type="checkbox"
                                            className='create-check-box'
                                            id={each.id}
                                            onClick={(event) => { this.toggleCheckBox(each, event) }}
                                            onChange={() => { }}
                                        />
                                        <label htmlFor={each.id} className='each-todo d-flex justify-content-between'>
                                            <li>{each.givenTodo}</li>
                                            <p onClick={() => { this.deletion(each.id) }} className='text-danger'>X</p>
                                        </label>

                                    </div>
                                )
                            })
                        }

                    </ul>

                    <footer className="end-part" id="end-part">

                        <h4 className='option' id="option">{todosCount} items left</h4>
                        <FooterPart
                            handelFilter={this.handelFilters}
                        />
                    </footer>

                </section>
            </div>
        );
    }
}

export default TodoApp;