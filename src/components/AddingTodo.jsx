import React, { Component } from 'react';
import './main.css'

class AddNewTodo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            input: ''
        }
    }

    submitText = (e) => {
        e.preventDefault()
        this.props.handelNewTodo(this.state.input)
        this.setState({
            input:''
        })
    }

    handleChange = (e) => {
        this.setState({
            input: e.target.value
        })
    }

    render() {
        return (
            <>
                <form onSubmit={this.submitText} >
                    <input type="text"
                        onChange={this.handleChange}
                        name="inputTodo"
                        placeholder='Create new todo...'
                        value= {this.state.input}
                        className='user-input'
                    />
                </form>
            </>
        );
    }
}

export default AddNewTodo;