import React, { Component } from 'react';

const buttonsOperation = [
    {
        text: 'All',
        operation: 'showAll'
    },
    {
        text: 'Active',
        operation: 'showActive'
    },
    {
        text: 'Complete',
        operation: 'showComplete'
    }
]

class FooterPart extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <>
                <div className="sub-opt">
                    {
                        buttonsOperation.map((eachBtn) => {
                            return (
                                <p onClick={()=>{this.props.handelFilters(eachBtn)}} key={eachBtn.text}>{eachBtn.text}</p>
                            )
                        })
                    }
                </div>
                <p onClick={this.props.clearCompleted} className="option m-1">Clear Completed</p>
            </>
        );
    }
}

export default FooterPart;


